#!/usr/bin/python3
"""
This script computes the relative pose error from the ground truth trajectory
and the estimated trajectory, robust to outliers, getting thresholds from yaml file
"""

# yapf: disable
import sys
import os
from os import path
sys.path.append( path.dirname( path.dirname( path.abspath(__file__) ) ) )
from utils import utils, syncronize
import numpy as np
import argparse
from typing import Tuple


"""
computes trajectory length and avg relative pose increment
"""
def calculate_abs_trajectory_stats(gt: np.ndarray):
    # ldg set display precision for floating point print
    np.set_printoptions(precision=10, suppress=True)
    # ldg initialize container for translational error
    relative_deltas_trans, relative_deltas_rot  = list(), list()
    for c in range(len(gt)-1):
        gt_isometry = gt[c]
        gt_isometry_next = gt[c+1]
        # ldg compute relative estimates
        relative_gt_isometry = utils.compute_relative_isometry(gt_isometry, gt_isometry_next)
        # add to relative poses
        relative_deltas_trans.append(np.abs(relative_gt_isometry[0:3, 3]))
        # relative_deltas_rot.append(relative_gt_isometry[0:3, 0:3])
    total_motion = np.sum(relative_deltas_trans, axis=0)
    # print("total trajectory length (over separate axis): % [m]"%(total_motion))
    print("total trajectory length: %f [m]"%(np.sum(total_motion)))
    # print("relative pose error | %s | std %f %s" % (v[2], np.std(error), v[3]))

    
    
     


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='''
    This script computes the relative pose error from the ground truth trajectory and the estimated trajectory. ''')
    parser.add_argument('--gt_file', help='ground-truth trajectory file (format: "timestamp tx ty tz qx qy qz qw")')
    parser.add_argument('--estimate_file', help='estimated trajectory file, to check delt transform after association (format: "timestamp tx ty tz qx qy qz qw")')
    parser.add_argument('--offset', help='time offset added to the timestamps of the estimate file (default: 0.0)', default=0.0)
    parser.add_argument('--max_difference', help='maximally allowed time difference for matching entries (default: 0.02)', default=0.02)
    parser.add_argument('--verbose', help='print all evaluation data (otherwise, only the mean translational error measured in meters will be printed)', action='store_true')
    args = parser.parse_args()

    # ldg read gt ground truth and estimate trajectory files
    gt_list = utils.read_file_list(args.gt_file)
    estimate_list = utils.read_file_list(args.estimate_file)

    # ldg sorting to timestamp in case these are not
    matches = syncronize.syncronize(gt_list, estimate_list, float(
        args.offset), float(args.max_difference))
    if len(matches) < 2:
        sys.exit("couldn't find matching timestamp pairs between groundtruth and estimated trajectory! Did you choose the correct sequence?")

    
    # ldg create a matrix of 7xn where n is the number of poses for gt trajectory
    gt_trajectory = np.matrix([[float(value) for value in gt_list[t_a][0:]] for t_a, _ in matches]).transpose()
    gt_trajectory = [utils.v2t(pose.A[0]) for pose in gt_trajectory.transpose()]
    calculate_abs_trajectory_stats(gt_trajectory)
    
    
    
    
    

    