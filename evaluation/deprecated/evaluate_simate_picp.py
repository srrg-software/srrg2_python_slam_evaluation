#!/usr/bin/python3

# Requirements:
# pip3 install python-argparse
# pip3 install numpy
# pip3 install matplotlib


"""
This script computes the absolute trajectory error from the ground truth
trajectory and the estimated trajectory

references:
    https://www.researchgate.net/publication/306150265_Closed-form_solution_of_absolute_orientation_using_orthonormal_matrices
    http://users.ics.forth.gr/~lourakis/publ/2016_icpr.pdf
    https://www.researchgate.net/publication/224378053_Least-squares_fitting_of_two_3-D_point_sets_IEEE_T_Pattern_Anal
    https://vision.in.tum.de/data/datasets/rgbd-dataset/tools
"""

import sys
sys.path.append('../')
from utils import utils, syncronize
import numpy as np
import argparse
from typing import Tuple
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
from matplotlib.patches import Ellipse

def error_and_jacobian(X, p, z):
    t = np.matrix(X[0:3, 3]).transpose() 
    R = X[0:3, 0:3]
    s = 1.0 / X[3, 3]
    z_hat = s*(R.dot(p) + t)
    e = z_hat - z
    J = np.zeros((3, 7))
    J[0:3, 0:3] = np.identity(3)
    J[0:3, 3:6] = utils.skew(-z_hat)
    J[0:3,   6] = np.ravel(-z_hat)
    return e, J


def align(gt: np.ndarray, estimate: np.ndarray, num_iterations: int, damping: float, kernel_threshold: float) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """Align two trajectories using the method of Horn (closed-form) based on SVD
        https://www.researchgate.net/publication/306150265_Closed-form_solution_of_absolute_orientation_using_orthonormal_matrices
        http://users.ics.forth.gr/~lourakis/publ/2016_icpr.pdf
        https://www.researchgate.net/publication/224378053_Least-squares_fitting_of_two_3-D_point_sets_IEEE_T_Pattern_Anal

    Input:
    gt        --  gt trajectory (3xn)
    estimate  --  estimate trajectory (3xn)

    Output:
    R       -- rotation matrix (3x3)
    t       -- translation vector (3x1)
    error_t -- translational error per point (nx0)
    """
    # ldg set display precision for floating point print
    np.set_printoptions(precision=3, suppress=True)
    gt_centroid = gt.mean(1)
    estimate_centroid = estimate.mean(1)
    # ldg refer all measurament to the centroids
    gt_zerocentered = gt - gt_centroid
    estimate_zerocentered = estimate - estimate_centroid
    # initialize stuff
    init_guess = [0, 0, 0, 0, 0, 0, 0]
    X = utils.v2s(init_guess)
    chi_stats = np.zeros((num_iterations))
    num_inliers = np.zeros((num_iterations))
    for iteration in range(num_iterations):
        H = np.zeros((7, 7))
        b = np.zeros((7, 1))
        # ldg iterate on the columns (x,y,z) of gt
        for column in range(gt.shape[1]):
            p = gt[:, column]
            z = estimate[:, column] 
            e, J = error_and_jacobian(X, p, z)
            chi = e.transpose().dot(e)
            if (chi > kernel_threshold):
                e *= np.sqrt(kernel_threshold/chi)
                chi = kernel_threshold
            else:
                num_inliers[iteration] += 1

            chi_stats[iteration] += chi 
            H += J.transpose().dot(J)
            b += J.transpose().dot(e)
        
        H += np.identity(7) * damping
        dx = np.linalg.solve(-H, b)
        X = multiply_sim(utils.v2s(np.ravel(dx)), X)
        # X = np.dot(utils.v2s(np.ravel(dx)), X)
        print("X\n", X)
        print("Error: ", chi_stats[iteration])
    
    print("initial error: ", chi_stats[0])
    print("final error: ", chi_stats[iteration])

    R = X[0:3, 0:3]
    t = estimate_centroid - (R * gt_centroid)
    # t = np.reshape(X[0:3, 3], (3, 1))
    s = float(X[3, 3])
    
    gt_aligned = s*(R * gt + t)
    # ldg find error alignment error
    alignment_error = gt_aligned - estimate
    # ldg elementwise multiplication to square matrix, always 3xn matrix 
    squared_alignment_error = np.multiply(alignment_error, alignment_error)
    # ldg sum columnwise, we have 1xn vector, squeeze on x, y, z components
    sum_squared_alignment_error = np.sum(squared_alignment_error, 0)
    # sqrt trans error and reshape to column vector (n,) for consistency 
    error_t = np.sqrt(sum_squared_alignment_error).A[0]
    
    print("final X\n", X)
    return s, R, t, error_t

    # sys.exit(0)


#    -0.62764   -0.77844    0.01002   15.14228
#     0.77848   -0.62767   -0.00064    3.31101
#     0.00679    0.00740    0.99995   -0.17737
#     0.00000    0.00000    0.00000    1.15995

# [[-0.628  0.778  0.007  7.737]
#  [-0.778 -0.628  0.007 13.283]
#  [ 0.01  -0.001  1.     0.024]
#  [ 0.     0.     0.     1.   ]]

# res 2 sim mult
#  [[-0.627  0.779  0.007  5.949]
#  [-0.779 -0.627  0.007 11.897]
#  [ 0.01  -0.001  1.     0.024]
#  [ 0.     0.     0.     1.16 ]]

# res 1 np dot sim 
#  [[-0.628  0.778  0.007  5.985]
#  [-0.778 -0.628  0.007 11.964]
#  [ 0.01  -0.001  1.     0.024]
#  [ 0.     0.     0.     0.863]]

            


def multiply_sim(sim1, sim2):
    sim_result = np.identity(4)
    sim_result[0:3, 0:3] = np.dot(sim1[0:3, 0:3], sim2[0:3, 0:3])
    sim_result[0:3, 3] = np.dot(sim1[0:3, 0:3], sim2[0:3, 3]) + (1.0/sim2[3, 3] * sim1[0:3, 3])
    sim_result[3, 3] = (1.0/sim1[3, 3]) * (1.0/sim2[3, 3])
    return sim_result

    # ldg align gt on estimate data
    # gt_aligned = (R * gt) + t
    # ldg find error alignment error
    # alignment_error = gt_aligned - estimate
    # # ldg elementwise multiplication to square matrix, always 3xn matrix 
    # squared_alignment_error = np.multiply(alignment_error, alignment_error)
    # # ldg sum columnwise, we have 1xn vector, squeeze on x, y, z components
    # sum_squared_alignment_error = np.sum(squared_alignment_error, 0)
    # # sqrt trans error and reshape to column vector (n,) for consistency 
    # error_t = np.sqrt(sum_squared_alignment_error).A[0]
    # return R, t, error_t
    # return 

if __name__ == "__main__":
    # parse command line
    parser = argparse.ArgumentParser(description='''This script computes the absolute trajectory error from the ground truth trajectory and the estimated trajectory.''')
    parser.add_argument('--gt_file', help='ground truth trajectory (format: timestamp tx ty tz qx qy qz qw)', default='/home/ldg/Desktop/giardiello_calib/bag_2_to_associate/gt_trajectory_sorted.txt')
    parser.add_argument('--estimate_file', help='estimated trajectory (format: timestamp tx ty tz qx qy qz qw)', default='/home/ldg/Desktop/giardiello_calib/bag_2_to_associate/camera_trajectory_sorted.txt')
    parser.add_argument('--offset', help='time offset added to the timestamps of the estimate file (default: 0.0)', default=0.0)
    parser.add_argument('--scale', help='scaling factor for the estimate trajectory (default: 1.0)', default=1.0)
    parser.add_argument('--max_difference', help='maximally allowed time difference for matching entries (default: 0.02)', default=0.02)
    parser.add_argument('--save', help='save aligned estimate trajectory to disk (format: stamp2 x2 y2 z2)')
    parser.add_argument('--plot', help='plot the gt and the aligned estimate trajectory to an image (format: png)')
    parser.add_argument('--verbose', help='print all evaluation data (otherwise, only the RMSE absolute translational error in meters after alignment will be printed)', action='store_true')
    args = parser.parse_args()

    # ldg read gt ground truth and estimate trajectory files
    gt_list = utils.read_file_list(args.gt_file)
    estimate_list = utils.read_file_list(args.estimate_file)

    # ldg sorting to timestamp in case these are not
    matches = syncronize.syncronize(gt_list, estimate_list, float(args.offset), float(args.max_difference))
    if len(matches) < 2:
        sys.exit("Couldn't find matching timestamp pairs between groundtruth and estimated trajectory! Did you choose the correct sequence?")
    # ldg create a matrix of 3xn where n is the number of poses for gt trajectory
    gt_xyz = np.matrix([[float(value) for value in gt_list[t_a][0:3]] for t_a, _ in matches]).transpose()
    # ldg create a matrix of 3xn where n is the number of poses for estimate trajectory
    estimate_xyz = np.matrix([[float(value)*float(args.scale) for value in estimate_list[t_b][0:3]] for _, t_b in matches]).transpose()
    # ldg get rotationtrans_error, translation for alignement and translation error vector
    s, R, t, trans_error = align(estimate_xyz, gt_xyz, 100, 1, 20)
    # align estimate trajectory on gt trajectory
    estimate_xyz_aligned = s * (R * estimate_xyz + t)
    
    # ldg operate in all the poses, not only the syncronized ones
    gt_timestamps = list(gt_list.keys())
    estimate_timestamps = list(estimate_list.keys())
    gt_timestamps.sort()
    estimate_timestamps.sort()
    gt_xyz_full = np.matrix([[float(value) for value in gt_list[t][0:3]] for t in gt_timestamps]).transpose()
    estimate_xyz_full = np.matrix([[float(value)*float(args.scale) for value in estimate_list[t][0:3]] for t in estimate_timestamps]).transpose()
    estimate_xyz_full_aligned = s * (R * estimate_xyz_full + t)
    
    if(args.verbose):
        print("compared_pose_pairs %d pairs" % (len(trans_error)))
        print("absolute translation error | rmse %f [m]" % np.sqrt(np.dot(trans_error, trans_error) / len(trans_error)))
        print("absolute translation error | mean %f [m]" % np.mean(trans_error))
        print("absolute translation error | median %f [m]" % np.median(trans_error))
        print("absolute translation error | std %f [m]" % np.std(trans_error))
        print("absolute translation error | min %f [m]" % np.min(trans_error))
        print("absolute translation error | max %f [m]" % np.max(trans_error))
    else:
        print("absolute translation error | rmse %f [m]" % np.sqrt(np.dot(trans_error, trans_error) / len(trans_error)))

    if(args.save):
        file = open(args.save, "w")
        file.write("\n".join(["%f " % stamp+" ".join(["%f" % d for d in line]) for stamp, line in zip(estimate_timestamps, estimate_xyz_full_aligned.transpose().A)]))
        file.close()

    if(args.plot):
        plot_dic = {0 : '_estimate_vs_aligned', 1 : '_estimate_vs_aligned_with_difference', 2 : '_estimate_vs_aligned_vs_not_aligned'}
        for k, v in plot_dic.items():
            fig = plt.figure()
            ax = fig.add_subplot(111)
            if(k == 1):
                label = "difference"
                for (a, b), (x1, y1, z1), (x2, y2, z2) in zip(matches, gt_xyz.transpose().A, estimate_xyz_aligned.transpose().A):
                    ax.plot([x1, x2], [y1, y2], '-', color="green", label=label)
                    label = ""
            utils.plot_trajectory(ax, gt_timestamps, gt_xyz_full.transpose().A, '-', "black", "ground truth")
            utils.plot_trajectory(ax, estimate_timestamps, estimate_xyz_full_aligned.transpose().A, '-', "red", "estimate aligned")
            if(k==2):
                utils.plot_trajectory(ax, estimate_timestamps, estimate_xyz_full.transpose().A, '-', "blue", "estimate")

            ax.legend()
            ax.set_xlabel('x [m]')
            ax.set_ylabel('y [m]')
            plt.savefig(args.plot + v, dpi=300)