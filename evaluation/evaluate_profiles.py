#!/usr/bin/python3

# Requirements:
# pip3 install python-argparse
# pip3 install numpy
# pip3 install matplotlib
"""
This script computes the absolute trajectory error from the ground truth
trajectory and the estimated trajectory

"""
# yapf: disable
import os
from os import path
import sys
sys.path.append( path.dirname( path.dirname( path.abspath(__file__) ) ) )
from utils import utils, syncronize
import numpy as np
import argparse
from typing import Tuple
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
from matplotlib.patches import Ellipse

if __name__ == "__main__":
    # parse command line
    parser = argparse.ArgumentParser(description='''This script computes the absolute trajectory error from the ground truth trajectory and the estimated trajectory.''')
    parser.add_argument('--gt_file', help='ground truth trajectory (format: timestamp tx ty tz qx qy qz qw)')
    parser.add_argument('--estimate_file', help='estimated trajectory (format: timestamp tx ty tz qx qy qz qw)')
    parser.add_argument('--offset', help='time offset added to the timestamps of the estimate file (default: 0.0)', default=0.0)
    parser.add_argument('--max_difference', help='maximally allowed time difference for matching entries (default: 0.02)', default=0.02)
    parser.add_argument('--scale', help='scaling factor for the estimate trajectory (default: 1.0)', default=1.0)
    parser.add_argument('--plot', help='plot the gt and the aligned estimate trajectory to an image (format: png)')
    args = parser.parse_args()

    # ldg read gt ground truth and estimate trajectory files
    gt_list = utils.read_file_list(args.gt_file)
    estimate_list = utils.read_file_list(args.estimate_file)

    # ldg sorting to timestamp in case these are not
    matches = syncronize.syncronize(gt_list, estimate_list, float(args.offset), float(args.max_difference))
    if len(matches) < 2:
        sys.exit("Couldn't find matching timestamp pairs between groundtruth and estimated trajectory! Did you choose the correct sequence?")
    # ldg create a matrix of 3xn where n is the number of poses for gt trajectory
    gt_xyz_trajectory = np.matrix([[float(value) for value in gt_list[t_a][0:3]] for t_a, _ in matches]).transpose()
    estimate_xyz_trajectory = np.matrix([[float(value) for value in estimate_list[t_b][0:3]] for _, t_b in matches]).transpose()

    # ldg operate in all the poses, not only the syncronized ones
    gt_timestamps = list(gt_list.keys())
    estimate_timestamps = list(estimate_list.keys())
    gt_timestamps.sort()
    estimate_timestamps.sort()
    gt_xyz = np.matrix([[float(value) for value in gt_list[t][0:3]] for t in gt_timestamps]).transpose()
    estimate_xyz = np.matrix([[float(value)*float(args.scale) for value in estimate_list[t][0:3]] for t in estimate_timestamps]).transpose()

    trans_profiles = ['x', 'y', 'z']
    if(args.plot):
        for prof in trans_profiles:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            utils.plot_profiles(ax, gt_timestamps, gt_xyz.transpose().A, prof, '-', "blue", prof + " gt")
            utils.plot_profiles(ax, estimate_timestamps, estimate_xyz.transpose().A, prof, '-', "red", prof + " estimate")
            ax.legend()
            ax.set_xlabel('t [s]')
            y_lab = prof + ' [m]'
            ax.set_ylabel(y_lab)
            plt.savefig(args.plot + "_" + prof, dpi=300)
