#!/usr/bin/python3
"""
This script computes the relative pose error from the ground truth trajectory
and the estimated trajectory, robust to outliers, getting thresholds from yaml file
"""

# yapf: disable
import sys
from scipy.signal import savgol_filter
import matplotlib.pylab as pylab
import matplotlib.pyplot as plt
import matplotlib
import time
import numpy as np
import random
import argparse
from os import path
sys.path.append( path.dirname( path.dirname( path.abspath(__file__) ) ) )
from utils import utils, syncronize
matplotlib.use('Agg')

# ldg init dict
THRESHOLDS = {
    'x': np.finfo(float).max,
    'y': np.finfo(float).max,
    'z': np.finfo(float).max,
    'roll': np.finfo(float).max,
    'pitch': np.finfo(float).max,
    'yaw': np.finfo(float).max,
}

def calculate_relative_pose_error(gt: np.ndarray, estimate: np.ndarray, stamps: np.ndarray, offset: np.ndarray):
    # ldg set display precision for floating point print
    np.set_printoptions(precision=10, suppress=True)
    relative_movement = list()
    # ldg initialize container for translational error
    trans_error, rot_error, delta_vec, trans_error_xyz, rot_error_rpy = list(), list(), list(), list(), list()
    for c in range(len(gt)):
        if c >= len(gt)-1:
            break

        gt_isometry = gt[c]
        gt_isometry_next = gt[c+1]
        # ldg get estimate current and next pose 4x4 isometry format
        estimate_isometry = estimate[c]
        estimate_isometry_next = estimate[c+1]
        # ldg compute relative estimates
        relative_gt_isometry = utils.compute_relative_isometry(gt_isometry, gt_isometry_next)
        # relative_estimate_isometry = utils.compute_relative_isometry(estimate_isometry, estimate_isometry_next)
        estimate_isometry = offset.dot(estimate_isometry).dot(np.linalg.inv(offset))
        estimate_isometry_next = offset.dot(estimate_isometry_next).dot(np.linalg.inv(offset))
        relative_estimate_isometry = utils.compute_relative_isometry(estimate_isometry, estimate_isometry_next)
        # ldg get relative time between poses
        delta_vec.append(stamps[c+1] - stamps[c])

        # ldg compute error between gt and estimate
        isometry_error = utils.compute_relative_isometry(relative_gt_isometry, relative_estimate_isometry)
        # ldg append translation and indipendent translation components to error container
        trans_error_xyz.append(isometry_error[0:3, 3])
        trans_error.append(np.linalg.norm(isometry_error[0:3, 3]))
        # ldg eval rotation matrices error quaternions
        quat = utils.rot2q(isometry_error[0:3, 0:3])
        # ldg convert to roll, pitch, yaw
        euler_angles = utils.q2euler(quat)
        # ldg append distance between quaternion and single angles
        rot_error_rpy.append(euler_angles)
        quat_gt = utils.rot2q(relative_gt_isometry[0:3, 0:3])
        quat_estimate = utils.rot2q(relative_estimate_isometry[0:3, 0:3])
        rot_error.append(utils.quaternion_angle(quat_gt, quat_estimate))

        relative_movement.append(np.linalg.norm(relative_gt_isometry[0:3, 3]))

    print("relative movement: ", np.mean(relative_movement))

    return np.matrix(trans_error).transpose(), np.matrix(rot_error).transpose(), np.matrix(delta_vec).transpose(), np.matrix(trans_error_xyz).transpose(), np.matrix(rot_error_rpy).transpose()


def reject_outliers_threshold(data, time, threshold=0.3):
    data_no_outliers = list()
    t = 0
    for el in data:
        if el < threshold:
            l = list()
            l.append(el)
            l.append(time[t])
            data_no_outliers.append(l)
        t += 1
    return np.matrix(data_no_outliers)

def subsample_trajectory(gt_trajectory, estimate_trajectory, min_delta, max_delta):
    # ldg subsample poses
    subasampled_gt_traj, subasampled_estimate_traj, indices = list(), list(), list()
    for i in range(len(gt_trajectory)):
        gt_isometry = gt_trajectory[i]
        estimate_isometry = estimate_trajectory[i]

        if not subasampled_gt_traj:
            subasampled_gt_traj.append(gt_isometry)
            subasampled_estimate_traj.append(estimate_isometry)
            indices.append(i)
            continue
        rel_t_gt = np.dot(np.linalg.inv(subasampled_gt_traj[-1]), gt_isometry)
        rel_t_estimate = np.dot(np.linalg.inv(subasampled_estimate_traj[-1]), estimate_isometry)
        if(np.linalg.norm(rel_t_gt[0:3, 3]) < min_delta or np.linalg.norm(rel_t_estimate[0:3, 3]) > max_delta):
            continue

        subasampled_gt_traj.append(gt_isometry)
        subasampled_estimate_traj.append(estimate_isometry)
        indices.append(i)
    return subasampled_gt_traj, subasampled_estimate_traj, indices


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='''
    This script computes the relative pose error from the ground truth trajectory and the estimated trajectory. ''')
    parser.add_argument('--gt_file', help='ground-truth trajectory file (format: "timestamp tx ty tz qx qy qz qw")')
    parser.add_argument('--estimate_file', help='estimated trajectory file (format: "timestamp tx ty tz qx qy qz qw")')
    parser.add_argument('--sensor_offset_name', help='name of the sensor offset name, has to be the sensor in the robot frame')
    parser.add_argument('--transform_file', help='yaml file where stored extrinsic poses, sensors in robot frame')
    parser.add_argument('--thresholds_file', help='yaml file where stored threshold error to discard')
    parser.add_argument('--max_difference', help='maximally allowed time difference for matching entries (default: 0.02)', default=0.2)
    parser.add_argument('--offset', help='time offset between ground-truth and estimated trajectory (default: 0.0)', default=0.0)
    parser.add_argument('--scale', help='scaling factor for the estimated trajectory (default: 1.0)', default=1.0)
    parser.add_argument('--plot', help='plot the result to a file (output format: png)')
    parser.add_argument('--verbose', help='print all evaluation data (otherwise, only the mean translational error measured in meters will be printed)', action='store_true')
    parser.add_argument('--min_trans', help='min threshold to discard pose, translation norm must be greater then this value')
    parser.add_argument('--max_trans', help='max threshold to discard pose, translation norm must be smaller then this value')

    args = parser.parse_args()

    # ldg parse thresholds

    if(args.thresholds_file):
        THRESHOLDS = utils.parse_thresholds_yaml(args.thresholds_file)
        print("thresholds values:")
        print(THRESHOLDS)

    # ldg store offset transformation locally if present
    sensor_in_robot = np.identity(4)
    if args.sensor_offset_name:
        sensor_in_robot = utils.parse_transforms_yaml(args.transform_file, args.sensor_offset_name)

    print("sensor in robot pose:")
    print(sensor_in_robot)
    print()

    # ldg read gt ground truth and estimate trajectory files
    gt_list = utils.read_file_list(args.gt_file)
    estimate_list = utils.read_file_list(args.estimate_file)

    # ldg sorting to timestamp in case these are not
    matches = syncronize.syncronize(gt_list, estimate_list, float(
        args.offset), float(args.max_difference))
    if len(matches) < 2:
        sys.exit("couldn't find matching timestamp pairs between groundtruth and estimated trajectory! Did you choose the correct sequence?")

    # ldg create a matrix of 7xn where n is the number of poses for gt trajectory
    gt_trajectory = np.matrix([[float(value) for value in gt_list[t_a][0:]] for t_a, _ in matches]).transpose()
    # ldg create a matrix of 7xn where n is the number of poses for estimate trajectory
    estimate_trajectory = np.matrix([[float(value)*float(args.scale) for value in estimate_list[t_b][0:]] for _, t_b in matches]).transpose()
    # ldg finally matrix format
    gt_trajectory = [utils.v2t(pose.A[0]) for pose in gt_trajectory.transpose()]
    estimate_trajectory = [utils.v2t(pose.A[0]) for pose in estimate_trajectory.transpose()]



    # ldg subsample trajectory with min and max translational variation
    if(args.min_trans and args.max_trans):
        print("min translation between poses ", args.min_trans, "max translation between poses ", args.max_trans)
        gt_trajectory, estimate_trajectory, indices = subsample_trajectory(gt_trajectory, estimate_trajectory, float(args.min_trans), float(args.max_trans))
        # ldg find avg delta from stamps
        gt_stamps_old = np.ravel(np.matrix(matches).transpose()[0])
        gt_stamps = list()
        for a in indices:
            gt_stamps.append(gt_stamps_old[a])
    else:
        print("taking all the poses, not subsampling!")
        gt_stamps = np.ravel(np.matrix(matches).transpose()[0])


    # ldg calculating relative pose error
    trans_error, rot_error, delta_vec, trans_error_xyz, rot_error_rpy = calculate_relative_pose_error(gt_trajectory, estimate_trajectory, gt_stamps, sensor_in_robot)
    # ldg make array 1-D
    trans_error = np.ravel(trans_error)
    rot_error = np.ravel(rot_error)
    delta_vec = np.ravel(delta_vec)

    original_poses = len(gt_stamps)

    # ldg remove outliers to overall containers
    rmse_t = np.sqrt(np.dot(trans_error, trans_error) / len(trans_error))
    trans_error_inliers_time = reject_outliers_threshold(trans_error, gt_stamps, rmse_t)
    trans_error = np.ravel(trans_error_inliers_time.transpose()[0])
    rot_error_inliers_time = reject_outliers_threshold(np.rad2deg(rot_error), gt_stamps, 4)
    rot_error = np.ravel(rot_error_inliers_time.transpose()[0])
    # ldg remove outliers to specific containers
    tx_error_inliers_time = reject_outliers_threshold(np.abs(np.ravel(trans_error_xyz[0])), gt_stamps, THRESHOLDS['x'])
    ty_error_inliers_time = reject_outliers_threshold(np.abs(np.ravel(trans_error_xyz[1])), gt_stamps, THRESHOLDS['y'])
    tz_error_inliers_time = reject_outliers_threshold(np.abs(np.ravel(trans_error_xyz[2])), gt_stamps, THRESHOLDS['z'])
    roll_error_inliers_time = reject_outliers_threshold(np.abs(np.ravel(np.rad2deg(rot_error_rpy[0]))), gt_stamps, THRESHOLDS['roll'])
    pitch_error_inliers_time = reject_outliers_threshold(np.abs(np.ravel(np.rad2deg(rot_error_rpy[1]))), gt_stamps, THRESHOLDS['pitch'])
    yaw_error_inliers_time = reject_outliers_threshold(np.abs(np.ravel(np.rad2deg(rot_error_rpy[2]))), gt_stamps, THRESHOLDS['yaw'])

    # ldg init dics for output and plotting
    plot_dic = {'trans_error_overall': [trans_error_inliers_time, 'blue', 'translational error [m]'],
                'rot_error_overall': [rot_error_inliers_time, 'red', 'rotational error [deg]']}

    error_dic = {'tx_error': [tx_error_inliers_time, 'blue', 'x translational error', '[m]', THRESHOLDS['x']],
                 'ty_error': [ty_error_inliers_time, 'blue', 'y translational error', '[m]', THRESHOLDS['y']],
                 'tz_error': [tz_error_inliers_time, 'blue', 'z translational error', '[m]', THRESHOLDS['z']],
                 'roll_error':  [roll_error_inliers_time, 'red', 'roll error (rx)', '[deg]', THRESHOLDS['roll']],
                 'pitch_error': [pitch_error_inliers_time, 'red', 'pitch error (ry)', '[deg]', THRESHOLDS['pitch']],
                 'yaw_error':   [yaw_error_inliers_time, 'red', 'yaw error (rz)', '[deg]', THRESHOLDS['yaw']]}

    if(not args.thresholds_file):
        print("NOT REMOVING OUTLIERS!")

    if args.verbose:
        print("total number of poses (with outliers) %d" % original_poses)
        print()
        print()
        print("overall translation")
        print("compared_pose_pairs %d pairs, removed percentaged of outliers %d[%%] over original size" % (len(trans_error), (original_poses-len(trans_error))/original_poses*100))
        print("relative pose error | translation | rmse %f [m]" % np.sqrt(np.dot(trans_error, trans_error) / len(trans_error)))
        print("relative pose error | translation | mean %f [m]" % np.mean(trans_error))
        print("relative pose error | translation | median %f [m]" % np.median(trans_error))
        print("relative pose error | translation | std %f [m]" % np.std(trans_error))
        print("relative pose error | translation | min %f [m]" % np.min(trans_error))
        print("relative pose error | translation | max %f [m]" % np.max(trans_error))
        # ldg rotation
        print(50*"=")
        print("overall rotation, error calculated with distance between quaternions")
        print("compared_pose_pairs %d pairs, removed percentaged of outliers %d[%%] over original size" % (len(rot_error), (original_poses-len(rot_error))/original_poses*100))
        print("relative pose error | rotation | rmse %f [deg]" % np.sqrt(np.dot(rot_error, rot_error) / len(rot_error)))
        print("relative pose error | rotation | mean %f [deg]" % np.mean(rot_error))
        print("relative pose error | rotation | median %f [deg]" % np.median(rot_error))
        print("relative pose error | rotation | std %f [deg]" % np.std(rot_error))
        print("relative pose error | rotation | min %f [deg]" % np.min(rot_error))
        print("relative pose error | rotation | max %f [deg]" % np.max(rot_error))
        print(50*"=")
        print("separate profiles")

        for k, v in error_dic.items():
            error = np.ravel(v[0].transpose()[0])
            print("compared_pose_pairs %d pairs, removed percentaged of outliers %d[%%] over original size" % (len(error), (original_poses-len(error))/original_poses*100))
            print("relative pose error | %s | rmse %f %s" % (v[2], np.sqrt(np.dot(error, error) / len(error)), v[3]))
            print("relative pose error | %s | mean %f %s" % (v[2], np.mean(error), v[3]))
            print("relative pose error | %s | median %f %s" % (v[2], np.median(error), v[3]))
            print("relative pose error | %s | std %f %s" % (v[2], np.std(error), v[3]))
            print("relative pose error | %s | min %f %s" % (v[2], np.min(error), v[3]))
            print("relative pose error | %s | max %f %s" % (v[2], np.max(error), v[3]))
            print(50*"-")

        # ldg delta time
        print(50*"=")
        print("time")
        print("relative pose error | delta time | mean %f [s]" % np.mean(delta_vec))
        print("relative pose error | delta time | max %f [s]" % np.max(delta_vec))
        print("relative pose error | delta time | min %f [s]" % np.min(delta_vec))
    else:
        print(np.mean(trans_error))

    if args.plot:
        # ldg plot overall errors
        for k, v in plot_dic.items():
            fig = plt.figure()
            # axes = plt.gca()
            ax = fig.add_subplot(111)
            stamps = np.ravel(v[0].transpose()[1])
            error = np.ravel(v[0].transpose()[0])
            rel_time = stamps[:-1] - stamps[0]
            # axes.set_ylim([0,50])
            ax.plot(rel_time, error[:-1], '-', color=v[1])
            yhat = savgol_filter(error[:-1], 21, 3)
            ax.plot(rel_time, yhat, '--', color='black')
            plt.legend(['actual profile', 'smoothed profile'])
            ax.set_xlabel('time [s]')
            ax.set_ylabel(v[2])
            plt.savefig(args.plot + k, dpi=300)
        # ldg plot separate profiles error
        for k, v in error_dic.items():
            fig = plt.figure()
            # axes = plt.gca()
            ax = fig.add_subplot(111)
            stamps = np.ravel(v[0].transpose()[1])
            error = np.ravel(v[0].transpose()[0])
            rel_time = stamps[:-1] - stamps[0]
            # axes.set_ylim([0,50])
            ax.plot(rel_time, error[:-1], '-', color=v[1])
            yhat = savgol_filter(error[:-1], 21, 3)
            ax.plot(rel_time, yhat, '--', color='black')
            legend =['actual profile', 'smoothed profile']
            if(args.thresholds_file):
                ax.plot(rel_time, np.ones(len(rel_time))*float(v[4]), ':', color='green')
                legend.append('outlier border')
            plt.legend(legend)
            ax.set_xlabel('time [s]')
            ax.set_ylabel(v[2] + ' ' + v[3])
            plt.savefig(args.plot + k, dpi=300)
