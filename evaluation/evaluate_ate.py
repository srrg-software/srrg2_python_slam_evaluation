#!/usr/bin/python3

# Requirements:
# pip3 install python-argparse
# pip3 install numpy
# pip3 install matplotlib
"""
This script computes the absolute trajectory error from the ground truth
trajectory and the estimated trajectory

references:
    https://www.researchgate.net/publication/306150265_Closed-form_solution_of_absolute_orientation_using_orthonormal_matrices
    http://users.ics.forth.gr/~lourakis/publ/2016_icpr.pdf
    https://www.researchgate.net/publication/224378053_Least-squares_fitting_of_two_3-D_point_sets_IEEE_T_Pattern_Anal
    https://vision.in.tum.de/data/datasets/rgbd-dataset/tools
"""

# yapf: disable
import sys
import os
from os import path
sys.path.append( path.dirname( path.dirname( path.abspath(__file__) ) ) )
from utils import utils, syncronize
import numpy as np
import argparse
from typing import Tuple
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
from matplotlib.patches import Ellipse

def align(gt: np.ndarray, estimate: np.ndarray, scale: float) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """Align two trajectories using the method of Horn (closed-form) based on SVD
        https://www.researchgate.net/publication/306150265_Closed-form_solution_of_absolute_orientation_using_orthonormal_matrices
        http://users.ics.forth.gr/~lourakis/publ/2016_icpr.pdf
        https://www.researchgate.net/publication/224378053_Least-squares_fitting_of_two_3-D_point_sets_IEEE_T_Pattern_Anal

    Input:
    gt        --  gt trajectory (3xn)
    estimate  --  estimate trajectory (3xn)

    Output:
    R       -- rotation matrix (3x3)
    t       -- translation vector (3x1)
    error_t -- translational error per point (nx0)
    """
    # ldg set display precision for floating point print
    np.set_printoptions(precision=3, suppress=True)
    # ldg obtain centroids from trajectories
    gt_centroid = gt.mean(1)
    estimate_centroid = estimate.mean(1)

    # ldg refer all measurament to the centroids
    gt_zerocentered = gt - gt_centroid
    estimate_zerocentered = estimate - estimate_centroid
    # ldg container for cross product sum
    H = np.zeros((3, 3))
    # ldg iterate on the columns (x,y,z) of gt
    for column in range(gt.shape[1]):
        # ldg compute outer product between 3x1 gt and 1x3 estimate slice
        H += np.dot(gt_zerocentered[:, column], estimate_zerocentered[:, column].transpose())
    # ldg decompose using singular value decomposition, s is a 3x1, np returns V as hermitian
    U, s, V_h = np.linalg.linalg.svd(H.transpose())
    # ldg initialize a 3x3 identity matrix
    S = np.matrix(np.identity(3))
    # ldg enforce matrix orthonormality for rotation matrix creation
    if(np.linalg.det(V_h) * np.linalg.det(U) < 0):
        # ldg theoretically algorthm fails
        print("Warning! align trajectories | det(V_h*U) < 0 | R is likely to be a reflection")
        print("if 3rd singular value is equal to zero, then solution is good! s : {0}".format(s))
        # ldg here we change sign of 3rd value of V_h
        S[2, 2] = -1
    # ldg obtaining rotation matrix from SVD
    R = U*S*V_h
    # ldg if scale is set from outside don't estimate it
    s = 1.0
    if(scale):
        s = float(scale)
    else:
        # ldg estimate scale
        s = np.sqrt(np.linalg.norm(estimate_zerocentered, 2)/np.linalg.norm(gt_zerocentered, 2))    
    # ldg obtaining translation from centroids
    t = estimate_centroid - (s * R * gt_centroid)
    # ldg align gt on estimate data
    gt_aligned = (s * R * gt) + t
    # ldg find error alignment error
    alignment_error = gt_aligned - estimate
    # ldg elementwise multiplication to square matrix, always 3xn matrix
    squared_alignment_error = np.multiply(alignment_error, alignment_error)
    # ldg sum columnwise, we have 1xn vector, squeeze on x, y, z components
    sum_squared_alignment_error = np.sum(squared_alignment_error, 0)
    # sqrt trans error and reshape to column vector (n,) for consistency
    error_t = np.sqrt(sum_squared_alignment_error).A[0]

    return s, R, t, error_t


if __name__ == "__main__":
    # parse command line
    parser = argparse.ArgumentParser(description='''This script computes the absolute trajectory error from the ground truth trajectory and the estimated trajectory.''')
    parser.add_argument('--gt_file', help='ground truth trajectory (format: timestamp tx ty tz qx qy qz qw)', default='/home/ldg/Desktop/giardiello_calib/bag_2_to_associate/gt_trajectory_sorted.txt')
    parser.add_argument('--estimate_file', help='estimated trajectory (format: timestamp tx ty tz qx qy qz qw)', default='/home/ldg/Desktop/giardiello_calib/bag_2_to_associate/camera_trajectory_sorted.txt')
    parser.add_argument('--offset', help='time offset added to the timestamps of the estimate file (default: 0.0)', default=0.0)
    parser.add_argument('--max_difference', help='maximally allowed time difference for matching entries (default: 0.02)', default=0.02)
    parser.add_argument('--save', help='save aligned estimate trajectory to disk (format: stamp2 x2 y2 z2)')
    parser.add_argument('--scale', help='scaling factor for the estimated trajectory (default: 1.0), if set not calculated internally')
    parser.add_argument('--plot', help='plot the gt and the aligned estimate trajectory to an image (format: png)')
    parser.add_argument('--threshold', help='threshold limit to check percentage of errors outliers', default=0.05)
    parser.add_argument('--verbose', help='print all evaluation data (otherwise, only the RMSE absolute translational error in meters after alignment will be printed)', action='store_true')
    args = parser.parse_args()

    # ldg read gt ground truth and estimate trajectory files
    gt_list = utils.read_file_list(args.gt_file)
    estimate_list = utils.read_file_list(args.estimate_file)

    # ldg sorting to timestamp in case these are not
    matches = syncronize.syncronize(gt_list, estimate_list, float(args.offset), float(args.max_difference))
    if len(matches) < 2:
        sys.exit("Couldn't find matching timestamp pairs between groundtruth and estimated trajectory! Did you choose the correct sequence?")
    # ldg create a matrix of 3xn where n is the number of poses for gt trajectory
    gt_xyz = np.matrix([[float(value) for value in gt_list[t_a][0:3]] for t_a, _ in matches]).transpose()
    # ldg create a matrix of 3xn where n is the number of poses for estimate trajectory
    estimate_xyz = np.matrix([[float(value) for value in estimate_list[t_b][0:3]] for _, t_b in matches]).transpose()
    # ldg get rotation, translation for alignement and translation error vector
    s, R, t, trans_error = align(estimate_xyz, gt_xyz, args.scale)
    # align estimate trajectory on gt trajectory
    estimate_xyz_aligned = (s * R * estimate_xyz) + t

    # ldg operate in all the poses, not only the syncronized ones
    gt_timestamps = list(gt_list.keys())
    estimate_timestamps = list(estimate_list.keys())
    gt_timestamps.sort()
    estimate_timestamps.sort()
    gt_xyz_full = np.matrix([[float(value) for value in gt_list[t][0:3]] for t in gt_timestamps]).transpose()
    estimate_xyz_full = np.matrix([[float(value) for value in estimate_list[t][0:3]] for t in estimate_timestamps]).transpose()
    estimate_xyz_full_aligned = (s * R * estimate_xyz_full) + t

    # ldg write scaling factor in the .bashrc to be retrieved from bash script
    # os.system('bash -c \'echo "export scale=%s" >> ~/.bashrc\'' %str(s))
    # os.system('bash -c \'source ~/.bashrc\'')

    if(args.verbose):
        print("alignment successfull, stats")
        print("rotation:")
        utils.print_matrix(R)
        print("translation:")
        utils.print_matrix(t)
        # do not change this value
        print("scale: ", s)
        print(10*"=")

        print("compared_pose_pairs %d pairs" % (len(trans_error)))
        print("absolute translation error | rmse %f [m]" % np.sqrt(np.dot(trans_error, trans_error) / len(trans_error)))
        print("absolute translation error | mean %f [m]" % np.mean(trans_error))
        print("absolute translation error | median %f [m]" % np.median(trans_error))
        print("absolute translation error | std %f [m]" % np.std(trans_error))
        print("absolute translation error | min %f [m]" % np.min(trans_error))
        print("absolute translation error | max %f [m]" % np.max(trans_error))
        # take care of thresholds
        print("="*15)
        threshold = float(args.threshold)
        outliers = list(filter(lambda err: err > threshold, trans_error))
        print("threshold limit %f" %threshold)
        print("number of errors over threshold %d" %len(outliers))
        print("percentage of errors over threshold %f [%%]" %(len(outliers)/len(trans_error)*100))
        print("mean of errors over threshold %f [m]" %np.mean(outliers))
        
    else:
        print("absolute translation error | rmse %f [m]" % np.sqrt(np.dot(trans_error, trans_error) / len(trans_error)))
        

    if(args.save):
        file = open(args.save, "w")
        file.write("\n".join(["%f " % stamp+" ".join(["%f" % d for d in line]) for stamp, line in zip(estimate_timestamps, estimate_xyz_full_aligned.transpose().A)]))
        file.close()

    if(args.plot):
        plot_dic = {0 : '_estimate_vs_aligned', 1 : '_estimate_vs_aligned_with_difference', 2 : '_estimate_vs_aligned_vs_not_aligned'}
        for k, v in plot_dic.items():
            fig = plt.figure()
            ax = fig.add_subplot(111)
            if(k == 1):
                label = "difference"
                for (a, b), (x1, y1, z1), (x2, y2, z2) in zip(matches, gt_xyz.transpose().A, estimate_xyz_aligned.transpose().A):
                    ax.plot([x1, x2], [y1, y2], '-', color="green", label=label)
                    label = ""
            utils.plot_trajectory(ax, gt_timestamps, gt_xyz_full.transpose().A, '-', "black", "ground truth")
            utils.plot_trajectory(ax, estimate_timestamps, estimate_xyz_full_aligned.transpose().A, '-', "red", "estimate aligned")
            if(k==2):
                utils.plot_trajectory(ax, estimate_timestamps, estimate_xyz_full.transpose().A, '-', "blue", "estimate")

            ax.legend()
            ax.set_xlabel('x [m]')
            ax.set_ylabel('y [m]')
            plt.savefig(args.plot + v, dpi=300)