# Evaluation scripts for SLAM pipes, ATE, RPE, single profiles, scale retrieving
Scripts taken and ajusted from TUM 

## Setup pkg and install dependencies
first clone and `cd` into `srrg2_python_slam_evaluation`

install pip

```python3 -m pip install --user --upgrade pip```

install a python virtual environments manager in this folder so that you don't fuck up the rest of your nice and clean py global env

```python3 -m pip install --user virtualenv```

create your custom virtualenv in this folder

```python3 -m venv srrg2_evalenv```

activate your env

```source srrg2_evaluation/bin/activate```

now you should see your environment activated before your usr name

```(srrg2_evalenv) usr@laptop:~ ...```

install requirements

```python3 -m pip install -r requirements.txt```

now you are ready to go

```cd evaluation```

## Run an evaluation

for instance run for Absolute Trajectory Error

```python3 evaluate_ate.py --estimate_file path/to/estimate_file --gt_file path/to/gt_file --verbose```

if you want to have info about `evaluate_ate.py` and what argument it can take run 

```python3 evaluate_ate.py -h```



before exit, close your virtualenv by running anywhere
```deactivate```



