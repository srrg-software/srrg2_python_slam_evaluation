#!/usr/bin/python3
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import yaml
import sys
matplotlib.use('Agg')

"""
This library provides user with generic functionality, like reading data and isometry manipulation
"""


def parse_transforms_yaml(filename: str, offset_name: str) -> np.ndarray:
    """
    Parsing yaml file transform matrix

    Input:
    filename -- the name and path of the file
    offset_name -- the name of the pose in the file e.g. sensor_in_robot

    Output:
    offset_matrix -- the isometry 4x4 containing the parsed extrinsinc pose
    """
    with open(filename, 'r') as stream:
        try:
            data_loaded = yaml.safe_load(stream)
            if offset_name in data_loaded:
                offset_vec = np.array(data_loaded[offset_name])
                offset_matrix = v2t(offset_vec)
            else:
                print('error offset name not contained in yaml file', filename)
                sys.exit(0)
        except yaml.YAMLError as exc:
            print(exc)
        return offset_matrix

def parse_thresholds_yaml(filename: str) -> dict:
    """
    Parsing yaml file

    Input:
    filename -- the name and path of the file
    
    Output:
    thresholds -- a dict containing thresholds value
    """
    with open(filename, 'r') as stream:
        try:
            data_loaded = yaml.safe_load(stream)
            thresholds = dict()
            for k, v in data_loaded.items():
                thresholds[k] = v
        except yaml.YAMLError as exc:
            print(exc)
        return thresholds

def print_matrix(matrix):
    print('\n'.join(['\t'.join([str(cell) for cell in row]) for row in matrix]))


def plot_trajectory(ax: plt.figure, stamps: 'list', traj: np.ndarray, style: str, color: str, label: str):
    """
    Plot a trajectory using matplotlib. 

    Input:
    ax -- the plot
    stamps -- time stamps (1xn)
    traj -- trajectory (3xn)
    style -- line style
    color -- line color
    label -- plot legend

    """
    stamps.sort()
    interval = np.median([s-t for s, t in zip(stamps[1:], stamps[:-1])])
    x = []
    y = []
    last = stamps[0]
    for i in range(len(stamps)):
        if stamps[i]-last < 2*interval:
            x.append(traj[i][0])
            y.append(traj[i][1])
        elif len(x) > 0:
            ax.plot(x, y, style, color=color, label=label)
            label = ""
            x = []
            y = []
        last = stamps[i]
    if len(x) > 0:
        ax.plot(x, y, style, color=color, label=label)


def plot_profiles(ax: plt.figure, stamps: list, traj: np.ndarray, coord: str, style: str, color: str, label: str):
    """
    Plot a profile, position coordinate over time 

    Input:
    ax -- the plot
    stamps -- time stamps (1xn)
    traj -- trajectory (3xn)
    coord -- coordinate to plot against time
    style -- line style
    color -- line color
    label -- plot legend

    """
    c_map = {'x': 0, 'y': 1, 'z': 2}
    stamps.sort()
    interval = np.median([s-t for s, t in zip(stamps[1:], stamps[:-1])])
    # max_time = np.max(stamps)
    max_coordinate = np.max(traj[:, c_map[coord]])
    coordinate, time = list(), list()
    last = stamps[0]
    for i in range(len(stamps)):
        if stamps[i]-last < 2*interval:
            coordinate.append(max_coordinate-traj[i][c_map[coord]])
            time.append(stamps[i]-stamps[0])
        elif len(coordinate) > 0:
            ax.plot(time, coordinate, style, color=color, label=label)
            label = ""
            coordinate, time = list(), list()
        last = stamps[i]
    if len(coordinate) > 0:
        ax.plot(time, coordinate, style, color=color, label=label)

def skew(v):
    S=[[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]]
    return np.asarray(S)

def Rx(rot_x):
    c = np.cos(rot_x)
    s = np.sin(rot_x)
    R = [[1,  0,   0],
         [0,  c,  -s],
         [0,  s,   c]]
    return np.asarray(R)

def Ry(rot_y):
    c = np.cos(rot_y)
    s = np.sin(rot_y)
    R = [[ c,  0,   s],
         [ 0,  1,   0],
         [-s,  0,   c]]
    return np.asarray(R)

def Rz(rot_z):
    c = np.cos(rot_z)
    s = np.sin(rot_z)
    R = [[c,  -s,   0],
         [s,   c,   0],
         [0,   0,   1]] 
    return np.asarray(R)

def v2s(v):
    v = np.asarray(v)
    sim = np.identity(4)
    sim = v2ta(v[0:6])
    sim[3, 3] = np.exp(v[6])
    return np.asarray(sim)


def angles2R(a):
    a = np.asarray(a)
    R = Rx(a[0]).dot(Ry(a[1])).dot(Rz(a[2]))
    return np.asarray(R)

def v2ta(v):
    v = np.asarray(v)
    T = np.identity(4)
    T[0:3, 0:3] = angles2R(v[3:6])
    T[0:3, 3] = v[0:3]
    return np.asarray(T)


def compute_relative_isometry(a, b):
    """
    Compute the relative 3D transformation between a and b.

    Input:
    a -- first pose (homogeneous 4x4 matrix)
    b -- second pose (homogeneous 4x4 matrix)

    Output:
    Relative 3D transformation from a to b.
    """
    return np.dot(np.linalg.inv(a), b)


def compute_angle(rotation: np.ndarray) -> float:
    """
    Compute the rotation angle from a 4x4 homogeneous matrix,
    this is useful when calculating the distance between two rotation matrices
    follows from Rodriguez

    Input:
    transform -- isometry pose (homogeneous 4x4 matrix)

    Output:
    A single float representing the angle value
    """
    return np.arccos(min(1, max(-1, (np.trace(rotation) - 1)/2)))


def v2t(vector_: list, alpha_=4.0) -> np.ndarray:
    """
    Generate a 4x4 homogeneous transformation matrix from a vector representation 

    Input:
    vector_ -- array consisting of (tx,ty,tz,qx,qy,qz,qw) where
               (tx,ty,tz) is the translational component and 
               (qx,qy,qz,qw) is the unit quaternion.

    Output:
    matrix  -- 4x4 homogeneous transformation matrix
    """
    eps = np.finfo(float).eps * alpha_
    t = vector_[0:3]
    q = np.array(vector_[3:8], dtype=np.float64, copy=True)
    nq = np.dot(q, q)
    if nq < eps:
        return np.array((
            (1.0,                 0.0,                 0.0, t[0])
            (0.0,                 1.0,                 0.0, t[1])
            (0.0,                 0.0,                 1.0, t[2])
            (0.0,                 0.0,                 0.0, 1.0)
        ), dtype=np.float64)
    q *= np.sqrt(2.0 / nq)
    q = np.outer(q, q)
    return np.array((
        (1.0-q[1, 1]-q[2, 2],     q[0, 1]-q[2, 3],     q[0, 2]+q[1, 3], t[0]),
        (q[0, 1]+q[2, 3], 1.0-q[0, 0]-q[2, 2],     q[1, 2]-q[0, 3], t[1]),
        (q[0, 2]-q[1, 3],     q[1, 2]+q[0, 3], 1.0-q[0, 0]-q[1, 1], t[2]),
        (0.0,                 0.0,                 0.0, 1.0)
    ), dtype=np.float64)


def q2euler(quat_: np.ndarray) -> np.ndarray:
    """
    Converts a quaternion representation [qx, qy, qz, qw] in to Euler angles roll, pitch, yaw 

    Input:
    quat_ -- vector 4x1 of the form [qx, qy, qz, qw] 

    Output:
    [roll, pitch, yaw]  -- vector 3x1
    """
    # roll (x-axis rotation)
    sinr_cosp = 2 * (quat_[3] * quat_[0] + quat_[1] * quat_[2])
    cosr_cosp = 1 - 2 * (quat_[0] * quat_[0] + quat_[1] * quat_[1])
    roll = np.arctan2(sinr_cosp, cosr_cosp)

    # pitch (y-axis rotation)
    sinp = 2 * (quat_[3] * quat_[1] - quat_[2] * quat_[0])
    if (np.abs(sinp) >= 1):
        # use 90 degrees if out of range
        pitch = np.copysign(np.pi() / 2, sinp)
    else:
        pitch = np.arcsin(sinp)

    # yaw (z-axis rotation)
    siny_cosp = 2 * (quat_[3] * quat_[2] + quat_[0] * quat_[1])
    cosy_cosp = 1 - 2 * (quat_[1] * quat_[1] + quat_[2] * quat_[2])
    yaw = np.arctan2(siny_cosp, cosy_cosp)

    return [roll, pitch, yaw]


def rot2q(R_: np.ndarray) -> np.ndarray:
    """
    Transforms a 3x3 rotation matrix to a quaternion representation [qx, qy, qz, qw] 

    Input:
    R_ -- 3x3 rotation matrix (yes SO3)

    Output:
    q  -- vector 4x1
    """
    q = [0, 0, 0, 0]
    t = np.trace(R_)
    if(t > 0):
        t = np.sqrt(t + 1)
        q[3] = 0.5 * t  # qw
        t = 0.5 / t
        q[0] = (R_[2, 1] - R_[1, 2]) * t  # qx
        q[1] = (R_[0, 2] - R_[2, 0]) * t  # qy
        q[2] = (R_[1, 0] - R_[0, 1]) * t  # qz
    else:
        i = 0
        if(R_[1, 1] > R_[0, 0]):
            i = 1
        if(R_[2, 2] > R_[i, i]):
            i = 2
        j = (i + 1) % 3
        k = (j + 1) % 3

        t = np.sqrt(R_[i, i] - R_[j, j] - R_[k, k] + 1)
        q[i] = 0.5 * t
        t = 0.5 / t
        q[3] = (R_[k, j] - R_[j, k]) * t  # qw
        q[j] = (R_[j, i] + R_[i, j]) * t
        q[k] = (R_[k, i] - R_[i, k]) * t
    return q


def quaternion_angle(q1_: np.ndarray, q2_: np.ndarray) -> float:
    """
    Calculate angle distance between two unit quaternions 

    Input:
    q1_  -- quaternion 1 vector 4x1
    q2_  -- quaternion 2 vector 4x1

    Output:
    angle  -- float angle error
    """
    d = q1_[3]*q2_[3] + q1_[0]*q2_[0] + q1_[1]*q2_[1] + q1_[2]*q2_[2]
    err = 2*(np.power(d, 2)) - 1
    theta = np.arccos(err)
    return theta


def normalize_quaternion(q_: np.ndarray) -> np.ndarray:
    """
    Normalize quaternion representation 

    Input:
    q_  -- maybe unnornamalized vector 4x1

    Output:
    q_  -- normalized vector 4x1
    """
    n = np.sqrt(q_[3]*q_[3] + q_[0]*q_[0] + q_[1]*q_[1] + q_[2]*q_[2])
    q_[3] /= n
    q_[0] /= n
    q_[1] /= n
    q_[2] /= n
    return q_


def t2v(matrix_: np.ndarray):
    """
    Transforms a 4x4 homogeneous matrix to vectorial format [tx, ty, tz, qx, qy, qz, qw]
    carefull is not minimal, includes qw 

    Input:
    matrix_ -- 4x4 homogeneous matrix (yes SE3)

    Output:
    vector  -- vector 7x1 (includes qw)
    """
    trans = matrix_[0:3, 3]
    q = rot2q(matrix_[0:3, 0:3])
    q = normalize_quaternion(q)
    if(q[3] > 0):
        q = np.matrix(q).A[0]
    else:
        q = -np.matrix(q).A[0]
    return [trans[0], trans[1], trans[2], q[0], q[1], q[2], q[3]]


def read_file_list(filename: str) -> dict:
    """
    Reads a trajectory from a text file and returns a dict of the format [timestamp] : values (x, y, ....)

    File format:
    The file format is "stamp v1 v2 v3 ...", where stamp denotes the time stamp (to be matched)
    and "v1 v2 v3.." is arbitary data (e.g., a 3D position and 3D orientation) syncronized to this timestamp.

    Input:
    filename -- File name

    Output:
    dict -- dictionary of (stamp, data) tuples

    """
    file = open(filename)
    data = file.read()
    lines = data.replace(",", " ").replace("\t", " ").split("\n")
    list = [[v.strip() for v in line.split(" ") if v.strip() != ""]
            for line in lines if len(line) > 0 and line[0] != "#"]
    list = [(float(l[0]), l[1:]) for l in list if len(l) > 1]
    return dict(list)


def txt_file_header() -> str:
    return "#\t\t" + "timestamp\t\t" + "x\t\t" + "y\t\t" + "z\t\t" + "qx\t\t" + "qy\t\t" + "qz\t\t" + "qw\n"


def write_trajectory_file(filename_: str, trajectory_: list):
    """
    Write a trajectory of the format (timestamp, x, y, ....)

    Input:
    filename_ -- File name
    trajectory_ -- List of messages (list)

    """
    outfile = open(filename_, "w")
    outfile.write(txt_file_header())
    for msg in trajectory_:
        for elem in msg:
            if elem == 0.0:  # cast to int zero more human readable
                elem = 0
            outfile.write(str(elem) + "\t")
        outfile.write("\n")
