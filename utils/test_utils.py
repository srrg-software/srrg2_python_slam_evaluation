from scipy.spatial.transform import Rotation as R
import utils as u
import numpy as np

np.set_printoptions(precision=5, suppress=True)

quaternion = np.array([0.1, 0.3, 0.2, 1.0])
t = np.array([0.2, 0.3, 0.4])

print(quaternion)
q_n = u.normalize_quaternion(quaternion)
print(q_n)

transform_v = np.concatenate((t, q_n), axis=0)
print(transform_v)
transform_m = u.v2t(transform_v)
print(transform_m)


q_from_rot = u.rot2q(transform_m[0: 3, 0: 3])
print(q_from_rot)


r = R.from_quat([0.1, 0.3, 0.2, 1.0])
print(r.as_matrix())
print(r.as_quat())





