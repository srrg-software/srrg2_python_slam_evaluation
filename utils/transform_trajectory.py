#!/usr/bin/python3

"""
This script transform a trajectory by a constant offset, 
i.e. useful when we want to add an extrinsic pose sensor in robot
"""

from matplotlib.patches import Ellipse
import matplotlib.pylab as pylab
import matplotlib.pyplot as plt
import argparse
import numpy as np
import sys
import utils
import matplotlib
matplotlib.use('Agg')

# -0.0158389 0.0753082 0.1 0.999779 -0.00925632 0.0163931  0.00935993

camera_in_orazio = np.matrix([[0.999475,	 -0.0199433, 	0.0255481,  0.0183025],
                              [0.01983,	      0.999792,	    0.0046796, -0.0629135],
                              [-0.0256362,   -0.00417052,   0.999663,   0.15],
                              [0,             0,            0,           1]]) 


if __name__ == "__main__":
    # parse command line
    parser = argparse.ArgumentParser(
        description='''This script transform a trajectory by a constant offset, i.e. useful when we want to add an extrinsic pose sensor in robot''')
    parser.add_argument(
        '--input_file', help='trajectory (format: timestamp tx ty tz qx qy qz qw)')
    parser.add_argument(
        '--output_file', help='estimated trajectory (format: timestamp tx ty tz qx qy qz qw)')
    parser.add_argument(
        '--plot', help='plot the gt and the aligned estimate trajectory to an image (format: png)')
    args = parser.parse_args()
    # ldg TODO get this from a txt file

    offset_vec = np.matrix([0.0183025,   -0.0629135, 0.15, -0.00221282,   0.0127978,  0.00994466, 0.999866]).A[0]
    # # ldg T 4x4 from vector
    offset = utils.v2t(offset_vec)
    # offset = camera_in_orazio
    # ldg read trajectory from txt file
    trajectory_list = utils.read_file_list(args.input_file)
    # ldg trasform all the poses in the original trajectory
    transformed_trajectory_list = dict()
    for stamp, pose_vec in trajectory_list.items():
        curr_pose = utils.v2t(np.matrix(pose_vec).A[0])
        transformed_pose = np.dot(curr_pose, offset)
        transformed_pose_vec = utils.t2v(transformed_pose)
        transformed_trajectory_list[stamp] = transformed_pose_vec

    # ldg output trajectory on txt file
    if(args.output_file):
        outfile = open(args.output_file, "w")
        outfile.write(utils.txt_file_header())
        for stamp, values in transformed_trajectory_list.items():
            outfile.write(str(stamp) + "\t" +
                          "\t".join([str(v) for v in values]) + "\n")
        outfile.close()

    if(args.plot):
        # ldg prepare data for plotting
        trajectory_timestamps = list(trajectory_list.keys())
        trajectory_timestamps.sort()
        trajectory_xyz = np.matrix([[float(value) for value in trajectory_list[t][0:3]]
                                    for t in trajectory_timestamps]).transpose()
        transformed_trajectory_timestamps = list(
            transformed_trajectory_list.keys())
        transformed_trajectory_timestamps.sort()
        transformed_trajectory_xyz = np.matrix(
            [[float(value) for value in transformed_trajectory_list[t][0:3]] for t in transformed_trajectory_timestamps]).transpose()

        fig = plt.figure()
        ax = fig.add_subplot(111)
        utils.plot_trajectory(ax, trajectory_timestamps, trajectory_xyz.transpose(
        ).A, '-', "blue", "original trajectory")
        utils.plot_trajectory(ax, transformed_trajectory_timestamps,
                              transformed_trajectory_xyz.transpose().A, '-', "red", "offset trajectory")

        ax.legend()
        ax.set_xlabel('x [m]')
        ax.set_ylabel('y [m]')
        plt.savefig(args.plot, dpi=300)
