#!/usr/bin/python3

# Requirements:
# pip3 install python-argparse
# pip3 install numpy


"""
This script allows to syncronize multiple text file (e.g. trajectories) based on timestamps
the files format can be like -> timestamp v1 v2 v3 ...
"""

import argparse
import sys
import os
import numpy
import utils
import numpy as np

def syncronize(first_list: list, second_list: list, offset: float, max_difference: float) -> list:
    """
    Syncronize two dictionaries of (stamp,data). As the time stamps never match exactly, we aim
    to find the closest match for every input tuple.

    Input:
    first_list -- first dictionary of (stamp,data) tuples
    second_list -- second dictionary of (stamp,data) tuples
    offset -- time offset between both dictionaries (e.g., to model the delay between the sensors)
    max_difference -- search radius for candidate generation

    Output:
    matches -- list of matched tuples ((stamp1,data1),(stamp2,data2))

    """
    first_keys = list(first_list.keys())
    second_keys = list(second_list.keys())
    potential_matches = [(abs(a - (b + offset)), a, b)
                         for a in first_keys
                         for b in second_keys
                         if abs(a - (b + offset)) < max_difference]
    potential_matches.sort()
    matches = list()
    for diff, a, b in potential_matches:
        if a in first_keys and b in second_keys:
            first_keys.remove(a)
            second_keys.remove(b)
            matches.append((a, b))

    matches.sort()
    return matches


if __name__ == '__main__':
    # parse command line
    parser = argparse.ArgumentParser(description='''This script takes two data files with timestamps and syncronizes them, outputs file name with _sorted extension in the same filepath''')
    parser.add_argument('--first_file', help='first text file (format: timestamp data)')
    parser.add_argument('--second_file', help='second text file (format: timestamp data)')
    parser.add_argument('--write_output', help='write sync output on different files', action='store_true')
    parser.add_argument('--transform_first', help='transform first wrt to second, else contrary', action='store_true')
    parser.add_argument('--transform_vector', help='transform only x, y, z components not rotation included (use this for ubisense pose)', action='store_true')
    parser.add_argument('--offset', help='time offset added to the timestamps of the second file (default: 0.0)', default=0.0)
    parser.add_argument('--max_difference', help='maximally allowed time difference for matching entries (default: 0.02)', default=0.02)
    args = parser.parse_args()

    first_list = utils.read_file_list(args.first_file)
    second_list = utils.read_file_list(args.second_file)

    matches = syncronize(first_list, second_list, float(args.offset), float(args.max_difference))
    
    # ldg set precision for readability
    np.set_printoptions(precision=3, suppress=True)
    first_stamp_0, second_stamp_0 = matches[0]
        
    if(args.transform_first):
        init_pose = utils.v2t(np.ravel(np.matrix(second_list[second_stamp_0])))
        first_pose = np.identity(4)
        if(args.transform_vector):
            first_pose[0:3, 3] = np.ravel(second_list[first_stamp_0])
        else:
            first_pose = utils.v2t(np.ravel(np.matrix(second_list[first_stamp_0])))
        print("The first trajectory is transformed wrt the second trajectory\n")
        print("First pose of second trajectory:\n")
        print(init_pose)
        for a, _ in matches:
            curr_pose = np.identity(4)
            if(args.transform_vector):
                curr_pose[0:3, 3] = np.ravel(first_list[a])
            else:
                curr_pose = utils.v2t(np.ravel(np.matrix(first_list[a])))
            curr_pose = np.dot(np.linalg.inv(first_pose), curr_pose)
            curr_pose_updated = np.dot(init_pose, curr_pose)
            # ldg update with transformed data
            # print("First list prev: ", first_list[a])
            first_list[a] = [str(v) for v in list(utils.t2v(curr_pose_updated))]
            # print("First list update: ", first_list[a])

    else:
        print(first_list[first_stamp_0])
        init_pose = utils.v2t(np.ravel(np.matrix(first_list[first_stamp_0])))
        first_pose = np.identity(4)
        if(args.transform_vector):
            first_pose[0:3, 3] = np.ravel(second_list[second_stamp_0])
        else:
            first_pose = utils.v2t(np.ravel(np.matrix(second_list[second_stamp_0])))
        print("The second trajectory is transformed wrt the first trajectory\n")
        print("First pose of first trajectory:\n")
        print(init_pose)
        for _, b in matches:
            curr_pose = np.identity(4)
            if(args.transform_vector):
                curr_pose[0:3, 3] = np.ravel(second_list[b])
            else:
                curr_pose = utils.v2t(np.ravel(np.matrix(second_list[b])))
            curr_pose = np.dot(np.linalg.inv(first_pose), curr_pose)
            curr_pose_updated = np.dot(init_pose, curr_pose)
            # ldg update with transformed data
            # print("First list prev: ", first_list[a])
            second_list[b] = [str(v) for v in list(utils.t2v(curr_pose_updated))]
            # print("First list update: ", first_list[a])
    
    # ldg print the first poses to check alignement
    print("First trajectory, pose[0]: ", first_list[first_stamp_0])
    print("Second trajectory, pose[0]: ", second_list[second_stamp_0])
    print(10*"=")

    # ldg out file names
    if(args.write_output):
        out_first_file = args.first_file.strip(".txt") + "_sync_transformed.txt"
        out_second_file = args.second_file.strip(".txt") + "_sync_transformed.txt"
        out_a = open(out_first_file, "w")
        out_b = open(out_second_file, "w")
        for a, b in matches:
            line_a = str(a) + "\t" + "\t".join(first_list[a]) + "\n"
            line_b = str(b-float(args.offset)) + "\t" + "\t".join(second_list[b]) + "\n"
            out_a.write(line_a)
            out_b.write(line_b)
        out_a.close()
        out_b.close()
